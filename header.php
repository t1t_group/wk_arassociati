<!DOCTYPE html>

<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?> >
<!--<![endif]-->

<head>

  <!-- META -->
	<meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-touch-fullscreen" content="YES">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=yes ">

  <!-- JQUERY -->
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  
  <!-- FAVICON -->
  <link rel="shortcut icon" type="image/x-icon" href="<?php echo bloginfo( 'stylesheet_directory' ); ?>/img/favicon.ico">

  <!--link href='https://fonts.googleapis.com/css?family=Lato:400,300italic,300,400italic,700,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=EB+Garamond' rel='stylesheet' type='text/css'-->

  <title><?php wp_title(''); ?></title>

    
  <?php 
    /* Test CSS */
    include("assets/import_header.php"); 

    global $woocommerce;
    global $javascript_append;

    wp_head();

  ?>

</head>

<?php 
$body_class = '';
if(is_front_page()){ $body_class = 'home';} 
?>
<body <?php body_class($body_class); ?>>
  <!--div class="logo_quadrato_container">
      <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home" class="logo_quadrato_sito">
            <?php get_template_part('img/svg/logo_quadrato.svg'); ?>
      </a>
  </div-->
  <header>
      <div class="wrap">
          <div class="logo_container">
              <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home" class="titolo_sito">
                    <?php get_template_part('img/svg/logo_inlinea.svg'); ?>
              </a>
          </div>

          <div class="wide_menu_container">
              <!-- MENU WIDE -->
              <?php include("block_menuwide.php"); ?> 
              
              <ul class="functional_menu">
                  <li class="language_selector_item"><?php echo custom_language_selector(); ?></li>
                  <li class="instagram_item"><?php echo do_shortcode('[elementor-template id="2839"]'); ?></li>
                  <li class="search_item"><?php echo do_shortcode('[elementor-template id="2845"]'); ?></li>
              </ul>
          </div>
          
          <!-- FUNCTIONAL MENU -->
          <div class="snd_menu_container">
              <!-- MENU MOBILE -->
              <?php include("block_menumobile.php"); ?>
          </div>
      </div>
  </header>