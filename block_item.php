
<a href="<?php echo get_the_permalink(); ?>" class="item-masonry">
        <?php
        $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium' );
        $url = $thumb['0']; 
        ?>
        <div class="item-img" style="background-image: url('<?php echo $url; ?>');">
                <!--img src="<?php echo $url; ?>" title="<?php the_title(); ?>" alt="<?php the_title(); ?>" /-->
        </div>
        <div class="item-text">
                <h4><?php the_title();?></h4>
                <?php
                // POSIZIONE GEOGRAFICA -> LOCALIZZAZIONE
                $term_list = wp_get_post_terms($post->ID, 'localizzazione');
                $term_list = array_reverse($term_list);
                foreach($term_list as $term){
                        
                        if($term->parent!=""){
                                if($localita_str!= ""){
                                        $localita_str .= '<span class="separatore">, </span>';
                                }
                                $localita = traducistringa($term->name, ICL_LANGUAGE_CODE );
                                $localita_str .= $localita;
                        }
                } ?>
                <h6 class="sottotitolo"><?php echo $localita_str; ?></h6>
        </div>
</a>