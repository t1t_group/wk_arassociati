<?php get_header(); ?>

<?php	
        
    ?>

<div id="contenuti">
	<div class="page_title wrapper">
		<?php $thisCat = $wp_query->get_queried_object(); ?>
		<h5><?php echo $thisCat->name; ?></h5>
	</div>
	<?php // $term = get_queried_object();?>
	<?php if (have_posts()) : ?>
	<div class="masonry-grid inner-grid"> 
		<?php 

		/* while (have_posts()) : the_post(); ?>
			<?php get_template_part('block_item'); ?>
		<?php endwhile; */

		//WK - 04/08/2022

		$queried_object = get_queried_object();

        $your_query = new WP_Query( array(
            'post_type' => array( 'progetto'),                
            'posts_per_page' => -1,
            'post_status' => 'publish',
            'orderby'=>'menu_order',
            'order' => 'ASC',
            'tax_query' => array (
		        array (
		            'taxonomy' => 'tipologia-di-progetto',
		            'field'    => 'term_id',
		            'terms' => $thisCat->term_id
		            )
		    	)
        ));

        while ( $your_query->have_posts() ) : $your_query->the_post();
            //print_r($post);
            get_template_part('block_item');

        endwhile;
        // reset post data (important!)
        wp_reset_postdata();

		?>
	</div>
	<?php endif;
	 
		

	 ?>
</div>
<?php get_footer(); ?>
