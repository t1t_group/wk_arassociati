<?php get_header(); ?>
<div id="contenuti">
	<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>
			<?php 
			$is_new_project = types_render_field("progetto-nuovo", array('post_id' => $post->ID, "raw"=>"true"));
			
			//if($is_new_project != 1){ ?>
				<?php 

				// HERO IMAGE

				$gallery = types_render_field("galleria-fotografica", array('post_id' => $post->ID, "raw"=>"true"));
				if(!empty($gallery)){
					$post_content = $gallery;
	    			preg_match('/\[gallery.*ids=.(.*).\]/', $post_content, $ids);
	    			$thumb_id = explode(",", $ids[1])[0];

				} else {
					$thumb_id = get_post_thumbnail_id($post->ID);
				}

				$thumb_m = wp_get_attachment_image_src( $thumb_id, 'large' );
				$thumb_w = wp_get_attachment_image_src( $thumb_id, 'full' );
				?>

				<div class="hero-img">
					<style>
						.hero-img { background-image:url('<?php echo $thumb_m['0'] ?>');}
						@media (min-width: 768px) {  .hero-img { background-image:url('<?php echo $thumb_m['0'] ?>'); } }
						@media (min-width: 1000px) {  .hero-img { background-image:url('<?php echo $thumb_w['0'] ?>'); } }
					</style>
				</div>


				<div class="wrapper content_wrapper">
					<h2><?php the_title(); ?></h2>

					<?php
					// POSIZIONE GEOGRAFICA -> LOCALIZZAZIONE
					$term_list = wp_get_post_terms($post->ID, 'localizzazione');
					$term_list = array_reverse($term_list);
					foreach($term_list as $term){
						
						if($term->parent!=""){
							if($localita_str!= ""){
								$localita_str .= '<span class="separatore">, </span>';
							}
							$localita = traducistringa($term->name, ICL_LANGUAGE_CODE );
							$localita_str .= $localita;
						}
					} ?>
					<h6><?php echo $localita_str; ?></h6>

					<?php the_content(); ?>
				</div>

				<div class="wrapper">
					<div class="wk-slider wk_horizontal">
						<ul class="slides">
							<!-- carico altre immagini della libreria -->
			        		<?php 
		        			if($gallery!=""){

			        			$post_content = $gallery;
			        			preg_match('/\[gallery.*ids=.(.*).\]/', $post_content, $ids);
			        			$array_id = explode(",", $ids[1]);
			        			$random_number=rand(0,100000);

			        			$numslide=1;
			        			foreach ($array_id as &$item) {

			        			  $thumb_s_b = wp_get_attachment_image_src( $item, 'medium' );
			        			  $thumb_m_b = wp_get_attachment_image_src( $item, 'large' );
			        			  $thumb_f_b = wp_get_attachment_image_src( $item, 'full' );
			        			  //$thumb_w_b = wp_get_attachment_image_src( $item, 'full' );

			        			  ?>
			        			    <li class="slide-<?= $numslide; ?>">
			        			    	<style>
			        			    	  .slide-<?= $numslide; ?> { background-image:url('<?php echo $thumb_s_b['0'] ?>');}
			        			    	  @media (min-width: 768px) {  .slide-<?= $numslide ?> { background-image:url('<?php echo $thumb_m_b['0'] ?>'); } }
			        			    	  @media (min-width: 1400px) {  .slide-<?= $numslide ?> { background-image:url('<?php echo $thumb_m_b['0'] ?>'); } }
			        			    	  @media (min-width: 1800px) {  .slide-<?= $numslide ?> { background-image:url('<?php echo $thumb_f_b['0'] ?>'); } }
			        			    	</style>
			        			    </li>
			        			    <?php 
			        			    $numslide++;
			        			}
			        		}
			        		?>
						</ul>
					</div>
				</div>

			<?php //} else {
			//	the_content();
			//} ?>

			<?php //include 'block_progetto_meta.php'; ?>

		<?php endwhile; ?>
	<?php endif; ?>
</div>
<?php get_footer(); ?>