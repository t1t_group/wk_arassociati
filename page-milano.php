<?php
/**
 * Template Name: Milano
 *
 */
 
get_header();
?>
<div id="contenuti">

    <div class="page_title wrapper">
        <h5><?php echo get_the_title(); ?></h5>
    </div>

    <div class="masonry-grid inner-grid">
    <?php
        /* ELEMENTI IN EVIDENZA */
        
        $your_query = new WP_Query( array(
             'post_type' => array( 'progetto' ),                
            'posts_per_page' => -1,
            'post_status' => 'publish',
            'orderby'=>'rand',
             'orderby'=>'rand',
              'order'=>'ASC',
              'tax_query' => array(
                array(
                            'taxonomy' => 'localizzazione',
                            'field' => 'slug',
                            'terms' => array ('milano-milan')
                        )          )
                 
         ));
        while ( $your_query->have_posts() ) : $your_query->the_post();
            
            get_template_part('block_item');

        endwhile;
        // reset post data (important!)
        wp_reset_postdata();
    ?>
    </div>
</div>

<?php
get_footer(); ?>

