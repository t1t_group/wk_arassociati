<?php get_header(); ?>
	<div id="contenuti">
		<?php if (have_posts()) : ?>
		<div class="masonry-grid inner-grid wrapper">
			<?php while (have_posts()) : the_post(); ?>
				<?php get_template_part('block_item'); ?>
			<?php endwhile; ?>
		</div>
		<?php endif; ?>
	</div>
<?php get_footer(); ?>