<div class="slider-home header" id="#slider-home">
	<ul class="slides">
	<?php

	$your_query = new WP_Query( array(
         'post_type' => array( 'progetto', 'post' ),                
        'posts_per_page' => 8,
        'post_status' => 'publish',
        'orderby'=>'rand',
         'meta_query' => array(
              array(
               'key' => 'wpcf-evidenzia-nello-slider-homepage',
               'value' => 1,
                  'compare' => '=',
             )
                             
         ),
         'orderby'=>'rand',
          'order'=>'ASC'
     ));
	$numeropost=0;

    while ( $your_query->have_posts() ) : $your_query->the_post();
        $custom_field = get_post_custom($post->ID);
        $url_news=get_permalink();
        $id_da_non_recuperare=$post->ID;


        $immagine_slider=types_render_field("immagine-homepage", array("raw"=>"true"));
        

        if($immagine_slider!=""){
            $immagine_slider_ID=pippin_get_image_id($immagine_slider);
            $thumb_s = wp_get_attachment_image_src( $immagine_slider_ID, 'medium' );
            $thumb_m = wp_get_attachment_image_src( $immagine_slider_ID, 'large' );
            $thumb_x = $immagine_slider;
        }else{
            $thumb_s = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium' );
            $thumb_m = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
            $thumb_x = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
            $thumb_x = $thumb_x[0];
        }
        


        // RICAVO SOTTOTITOLO
        $sottotitolo=types_render_field("sottotitolo", array("raw"=>"true"));
        $periodo=types_render_field("anno-di-realizzazione", array("raw"=>"true"));

        /*
        if($sottotitolo!=""){

        	$subtitle=$sottotitolo;

        }else{

        	if(get_post_type()=="progetto"){

        		$periodo=types_render_field("anno-di-realizzazione", array("raw"=>"true"));

        		$terms = get_the_terms($post->ID, 'localizzazione');
        		foreach($terms as $term){

        			$children = get_categories( array ('taxonomy' => 'localizzazione', 'parent' => $term->term_id ));

        		      if ( count($children) == 0 ) {
        		          // if no children, then echo the category name.
        		          $location= $term->name;
        		      }
        			
        		}

        		$location=traducistringa($location, ICL_LANGUAGE_CODE );
        		$subtitle=$location.' '.$periodo;

        	}else {

        		$subtitle=get_the_date('d F Y');

        	}
        }
        */

        ?>
        <li class="test slide-<?= $numeropost ?>">
        	<a href="<?php echo get_the_permalink(); ?>" class="title-slide">
        		<h1><?php the_title(); ?></h1>
        		<span class="data-slide"><?php echo sottotitolo($sottotitolo, $post->ID, $periodo); ?></span>
        	</a>
        	<style>
        	  .slide-<?= $numeropost; ?> { background-image:url('<?php echo $thumb_s['0'] ?>');}
        	  @media (min-width: 768px) {  .slide-<?= $numeropost; ?> { background-image:url('<?php echo $thumb_m['0'] ?>'); } }
        	  @media (min-width: 1400px) {  .slide-<?= $numeropost; ?> { background-image:url('<?php echo $thumb_m['0'] ?>'); } }
              @media (min-width: 1800px) {  .slide-<?= $numeropost; ?> { background-image:url('<?php echo $thumb_x ?>'); } }
        	</style>
        </li>
        

        <?php
        $numeropost++;

    endwhile;
    // reset post data (important!)
    wp_reset_postdata();
	?>
	</ul>
	<div id="scrolldown" class="scroll-home nomobile trigger">
        <svg width="24" height="61" viewBox="0 0 24 61" class="center-block svgscroll nomobile">
            <g sketch:type="MSLayerGroup" transform="translate(-483 -459) translate(483 459)" fill="none">
                <path d="M5 44.7l7 6.953 7-6.953" class="svgarrow sa1" stroke="#fff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" sketch:type="MSShapeGroup"></path>
                <path d="M3 50.66l9 8.94 9-8.94" class="svgarrow sa2" stroke="#fff" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" sketch:type="MSShapeGroup"></path>
            </g>
        </svg>             
	</div>
</div>


		
	