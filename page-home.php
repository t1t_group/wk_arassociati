<?php
/**
 * Template Name: Homepage
 *
 */
 
get_header();
?>

<div class="hero-slider-home wk-slider" style="overflow: hidden;">
    <?php 
    $slides = get_posts( array(
        'post_type' => 'slider-home',
        'posts_per_page' => -1,
        'post_status' => 'publish',
        'suppress_filters' => false,
        'orderby' => 'menu_order',
        'order' => 'ASC'
    ) );
    
    if(!empty($slides)){ ?>
        <ul class="slides">
        <?php $numslide = 0; ?>
        <?php foreach ( $slides as $slide ) { 
            $slide_img = types_render_field("immagine-wide", array('post_id' => $slide->ID, "raw"=>"true"));
            $slide_img_mobile = types_render_field("immagine-mobile", array('post_id' => $slide->ID, "raw"=>"true"));
            $link = types_render_field("link", array('post_id' => $slide->ID, "raw"=>"true"));
            $immagine_scura = types_render_field("immagine-scura", array('post_id' => $slide->ID, "raw"=>"true"));
            $link_video = types_render_field("slide-video", array('post_id' => $slide->ID, "raw"=>"true"));
            $slide_class = "";

            if($immagine_scura == 1){
                $slide_class=" slide-scura ";
            }

            if(empty($slide_img_mobile)){
                $slide_img_mobile = $slide_img;
            }

            //<iframe width="560" height="315" src="https://www.youtube.com/embed/XINlEYXA3k0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            ?>
            <?php if(!empty($link_video)) { ?>
                <li class="slide-item slide-<?= $numslide; ?> <?php echo $slide_class; ?>">
                    <a <?php if(!empty($link)){ ?> href="<?php echo $link; ?>" <?php } ?>>
                        <video autoplay loop muted playsinline>
                        <style>
                            .hero-slider-home video {
                                width: 100%;
                                height: 100%;
                                position: relative;
                                object-fit: cover;
                                object-position: center;
                            }
                        </style>
                          <source src="<?php echo $link_video; ?>" type="video/mp4">
                        Your browser does not support the video tag.
                        </video>
                        <div class="slide__caption">
                            <?php echo apply_filters( 'the_content', $slide->post_content ); ?>
                        </div>
                    </a>
                </li>
            <?php } else if(!empty($slide_img)){ ?>
                <li class="slide-item slide-<?= $numslide; ?> <?php echo $slide_class; ?>">
                    <a <?php if(!empty($link)){ ?> href="<?php echo $link; ?>" <?php } ?>>
                        <style>
                          .slide-<?= $numslide; ?> { background-image:url('<?php echo $slide_img_mobile; ?>');}
                          @media (min-width: 768px) {  .slide-<?= $numslide ?> { background-image:url('<?php echo $slide_img; ?>'); } }
                          @media (min-width: 1400px) {  .slide-<?= $numslide ?> { background-image:url('<?php echo $slide_img; ?>'); } }
                          @media (min-width: 1800px) {  .slide-<?= $numslide ?> { background-image:url('<?php echo $slide_img; ?>'); } }
                        </style>
                        <div class="slide__caption">
                            <?php echo apply_filters( 'the_content', $slide->post_content ); ?>
                        </div>
                    </a>
                </li>
            <?php } ?>
            <?php $numslide++; ?>
        <?php } ?>
        </ul>
    <?php } ?>
</div>

<div id="contenuti">
    <div class="masonry-grid homepage-grid">
    <?php

        /* ELEMENTI IN EVIDENZA */
        $id_evidenza = get_posts(array(
            'post_type' => array( 'progetto'),                
            'posts_per_page' => -1,
            'fields' => 'ids', 
            'post_status' => 'publish',
            'post__not_in' => array($id_da_non_recuperare),
            'meta_query' => array(
                  array(
                   'key' => 'wpcf-evidenzia-in-homepage',
                   'value' => 1,
                    'compare' => '=',
                 ),
            ) 
        ));

        $your_query = new WP_Query( array(
            'post_type' => array( 'progetto'),                
            'posts_per_page' => -1,
            'post_status' => 'publish',
            'post__in' => $id_evidenza,
            'meta_key' => 'wpcf-ordine', // 02/08/2022
            'meta_type' => 'NUMERIC', // 02/08/2022
            'meta_compare' => '>',
            'meta_value' => '-1',
            'orderby'=>'meta_value_num', // 02/08/2022
            'order' => 'DESC' 
        ));

        while ( $your_query->have_posts() ) : $your_query->the_post();
            //print_r($post);
            get_template_part('block_item');

        endwhile;
        // reset post data (important!)
        wp_reset_postdata();

    ?>
    </div>
</div>
<?php
get_footer(); ?>