<a href="#" class="mobile-menu" data-action="toggleMenu"><span class="fa fa-bars" data-action="toggleMenu"></span></a>
<nav class="menu_mobile_nav">
    <div class="nav_wrapper">
        <div class="logo_quadrato_container">
            <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home" class="logo_quadrato_sito">
                <?php get_template_part('img/svg/logo_quadrato.svg'); ?>
            </a>
        </div>
        <?php 
        /*  MENU MOBILE  */
        wp_nav_menu( array(
            'theme_location'  =>'menumobile' ,
            'container'       => '',
            'container_class' => false,
          )
        );
        ?>

        <ul class="functional_menu">
            <li class="language_selector_item"><?php echo custom_language_selector(); ?></li>
            <li class="instagram_item"><?php echo do_shortcode('[elementor-template id="2890"]'); ?></li>
            <li class="search_item"><?php echo do_shortcode('[elementor-template id="2845"]'); ?></li>
        </ul>
    </div>
</nav>