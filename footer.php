<?php
/**
 * The template for displaying the footer.
 *
 * Contains the body & html closing tags.
 *
 * @package HelloElementor
 */
/*
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

if ( ! function_exists( 'elementor_theme_do_location' ) || ! elementor_theme_do_location( 'footer' ) ) {
    get_template_part( 'template-parts/footer' );
}*/

if(is_single()){
    include 'block_progetto_meta.php'; 
}

?>

<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
    <div class="slides"></div>
    <h3 class="title"></h3>
    <a class="prev"></a>
	<a class="next"></a>
	<a class="close"><?php include 'img/svg/close_cross.svg'; ?></a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
</div>

<?php
/* FOOTER IN ELEMENTOR */
echo do_shortcode('[elementor-template id="2778"]'); 
?>

<?php 
include("assets/import_footer.php"); 

global $javascript_append;

echo '<script>' . $javascript_append . '</script>'; 

if(get_template() == 'wk_arassociati_dev'){ ?>
    <script> 
      console.log("****************************");
      console.log("*********** DEV ************");
      console.log("****************************");
    </script> 
<?php } ?>

<?php wp_footer(); ?> 

</body>
</html>