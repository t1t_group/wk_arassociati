/**
 *  REQUIRES
 **/

var gulp = require('gulp');
var repoWatch = require('gulp-repository-watch');
var gutil = require('gulp-util');
var git = require('gulp-git');
var runSequence = require('run-sequence');
var fs = require('fs');

// debugger
var cleanCSS = require('gulp-clean-css');
var plumber = require('gulp-plumber');
var notify = require('gulp-notify');
var jshint = require('gulp-jshint');
var uglify = require('gulp-uglify');
var sass = require('gulp-sass');
var concat = require('gulp-concat');

var inject = require('gulp-inject');
var prefix = require('gulp-autoprefixer');
var gulpif = require('gulp-if');
var prompt = require('gulp-prompt');
var rsync  = require('gulp-rsync');
var argv   = require('minimist')(process.argv);

var config = require('./dev/settings.json');

var plumberErrorHandler = { errorHandler: notify.onError({
    title: 'Gulp',
    message: 'Error: <%= error.message %>'
  })
};


function swallowError (error) {

  // If you want details of the error in the console
  //console.log(error.toString())

  this.emit('end')
}

function throwError(taskName, msg) {
    throw new gutil.PluginError({
        plugin: taskName,
        message: msg
    });
}


function css_concat(path){
    console.log(path + '.css generating...');
    var blob = "";
    for(var item in config.css[path]) {
        console.log('\tConcat file: ' + config.css[path][item]+'.css');
        blob = blob + fs.readFileSync('dev/css/' + config.css[path][item] + '.css').toString();
    }

    fs.writeFile('prod/css/' + path + '.css', blob, function (err) {
        if (err) throw err;
        console.log(path + '.css generated!');
    });
}


function get_css_lists(){
    var prefix = 'dev/css/';
    var suffix = '.css';
    var lists = [];
    var list = [''];

    for(var path in config.css){
        for(var item in config.css[path]){
            list.push(prefix + config.css[path][item] + suffix);
        }
        lists.push(list);
        list = [];
    }
    return lists;
}


gulp.task('deploy', ['sass-dev'], function() {
    //* Dirs and Files to sync
    //rsyncPaths = ['assets', 'prod', 'dev', 'font', 'img', 'js', './*.php','vc_templates', '!./style.css'];
    // Use gulp-rsync to sync the files
    runSequence(
        'sass-production',
        'uglify',
        'rsync-production', 
        function (error) {
            if (error) {
              console.log(error.message);
            } else {
              console.log('RELEASE FINISHED SUCCESSFULLY');
            }
        }
    );
});


gulp.task('rsync-dev',function(){
    //* Dirs and Files to sync
    rsyncPaths = ['assets', 'prod', 'font', 'img', 'js', './*.php', './style.css'];
    // Default options for rsync in staging side
    rsyncConf = {
        progress: false,
        incremental: true,
        relative: true,
        emptyDirectories: true,
        recursive: true,
        //port: config.server.port,
        //password: 'uWxTE5mb4xYhTJvKUH',
        clean: true,
        exclude: ['gulpfile.js'],
    };
    
    rsyncConf.hostname = config.server.host; // hostname
    rsyncConf.username = config.server.username; // ssh username
    rsyncConf.destination = config.server.dev_folder; // path where uploaded files go

    return gulp.src(rsyncPaths)
        .pipe(rsync(rsyncConf));
});


gulp.task('rsync-production',function(){
    //* Dirs and Files to sync
    rsyncPaths = ['assets', 'prod', 'font', 'img', 'js', './*.php', './style.css', 'widget', 'dev'];
    // Default options for rsync in production side
    rsyncConfProd = {
        progress: false,
        incremental: true,
        relative: true,
        emptyDirectories: true,
        recursive: true,    
        port: config.server.port,
        clean: true,
        exclude: ['gulpfile.js', 'css/dev', 'assets/jsons'],
    };

    rsyncConfProd.hostname = config.server.host; // hostname
    rsyncConfProd.username = config.server.username; // ssh username
    rsyncConfProd.destination = config.server.prod_folder;

    return gulp.src(rsyncPaths)
        .pipe(rsync(rsyncConfProd));
});



// COMPILE SASS IN DEVELOPMENT
gulp.task('sass-dev', function (){
    gulp.src(['./dev/scss/**/*.scss','./dev/scss/*.scss', './dev/scss/_config/*'])
        .pipe(plumber(plumberErrorHandler))
        .pipe(sass({errLogToConsole: true})).on('error', swallowError)
        .pipe(prefix(
          "last 1 version", "> 1%", "ie 8", "ie 7"
          ))
        .pipe(gulp.dest('./dev/css/.'));
});


// COMPILE SASS IN DEVELOPMENT
gulp.task('sass-config', function (){
    gulp.src(['./dev/scss/config/config.scss'])
        .pipe(plumber(plumberErrorHandler))
        .pipe(sass({errLogToConsole: true})).on('error', swallowError)
        .pipe(prefix(
          "last 1 version", "> 1%", "ie 8", "ie 7"
          ))
        .pipe(gulp.dest('./dev/css/.'));
});


gulp.task('sass-config-seq', function(){
    runSequence(
        'sass-config',
        'sass-dev', 
        function (error) {
            if (error) {
              console.log(error.message);
            } else {
              console.log('RELEASE FINISHED SUCCESSFULLY');
            }
        }
    );
});


// COMPILE SASS IN PRODUCTION
gulp.task('sass-production', function (){

    for(var path in config.css){
        css_concat(path);
        gulp.src('prod/css/'+ path +'.css')
            .pipe(cleanCSS({debug: true}, (details) => {
                console.log(`${details.name}:\t\t${details.stats.originalSize} b  -->  ${details.stats.minifiedSize} b`);
            }))
            .pipe(gulp.dest('./prod/css/.')); 
    }
});



/****** TODO: abilitare parametro -v per output compilazione */
// Uglify JS
gulp.task('uglify', function(){
    gulp.src('./dev/js/*.js')
        .pipe(plumber(plumberErrorHandler))
        .pipe(jshint())
        //.pipe(jshint.reporter('default'))
        .pipe(uglify())
        .pipe(concat('theme.js'))
        .pipe(gulp.dest('./prod/js/.')).on('error', swallowError);
});




gulp.task('default', function () {
    
    var sass_builder = gulp.watch(['./dev/scss/*.scss','./dev/scss/**/*.scss']);
    var css_watch = gulp.watch(['./dev/css/*.css','./dev/css/**/*.css']);
    var sass_config = gulp.watch(['./dev/scss/config/**/*.scss'], ['sass-config-seq']);

    //*********** COMPILE SASS
    sass_builder.on('change', function (f){
        
        var percorso_finale = get_relative_path(f.path);
        console.log(percorso_finale);
        gulp.src([percorso_finale])
            .pipe(plumber(plumberErrorHandler))
            .pipe(sass({errLogToConsole: true}))
            .pipe(prefix(
              "last 1 version", "> 1%", "ie 8", "ie 7"
              ))
            .pipe(gulp.dest(get_destination_path(percorso_finale)))
            .on('error', swallowError);
            //console.log("Compilazione: "+percorso_finale+"\n");            
    });

    //*********** UPLOAD CSS
    css_watch.on('change',function(f){

        var percorso_finale = get_relative_path(f.path);

        rsyncPaths = [percorso_finale];
        // Default options for rsync in staging side
        rsyncConf = {
            progress: true,
            incremental: true,
            relative: true,
            emptyDirectories: true,
            recursive: true,
            port: "22",
            clean: true,
            exclude: ['./style.css'],
        };

        rsyncConf.hostname = config.server.host; // hostname
        rsyncConf.username = config.server.username; // ssh username
        rsyncConf.destination = config.server.dev_folder; // path where uploaded files go

        return gulp.src(rsyncPaths).pipe(rsync(rsyncConf)).on('error', swallowError);
        
    });


});


function get_destination_path(path){
    var path_a = path.split('/');
    var filename = path_a[path_a.length-1];
    var res = path.replace('/dev/scss','/dev/css');
    console.log(res.replace(filename, '.'));
    return res.replace(filename, '.');
}


function get_relative_path(path){
    var path_a = path.split("/dev");
    return './dev'+path_a[1];
}
