<?php

/***
*** ESEMPIO DICHIARAZIONE TASSONOMIA
***
add_action( 'init', 'create_tax' );

function create_tax() {
    register_taxonomy(
        'accomodation-type',
        'accomodation',
        array(
            'label' => __( 'Type of accommodation' ),
            'public' => true,
            'rewrite' => 'slug',
            'hierarchical' => false,
        )
    );
}
/***
*** 
***/

?>