<!-- *** IMPORT_FOOTER *** -->

<!-- JS -->
<script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


<?php

if(is_user_logged_in()){
    $rand=rand(0,1000); ?>
    
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/dev/css/core__wordpress.css?v=<?php echo $rand; ?>">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/dev/css/third_parts/blueimpgallery.css?v=<?php echo $rand; ?>">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/dev/css/layout__home.css?v=<?php echo $rand; ?>">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/dev/css/layout__blog.css?v=<?php echo $rand; ?>">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/dev/css/layout__footer.css?v=<?php echo $rand; ?>">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/dev/css/layout__woocommerce.css?v=<?php echo $rand; ?>">
    
    <script src="<?php echo get_template_directory_uri(); ?>/dev/js/jquery.blueimp-gallery.min.js?v=<?php echo $rand; ?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fitvids/1.1.0/jquery.fitvids.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/dev/js/jquery.flexslider-min.js?v=<?php echo $rand; ?>"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/dev/js/isotope.pkgd.min.js?v=<?php echo $rand; ?>"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/dev/js/imagesLoaded.js?v=<?php echo $rand; ?>"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/dev/js/respond.min.js?v=<?php echo $rand; ?>"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/dev/js/sito.js?v=<?php echo $rand; ?>"></script>

<?php 

} else { 
    $rand=rand(0,1000); ?>
    
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/prod/css/style.css?v=<?php echo $rand; ?>">
    <script src="<?php echo get_template_directory_uri(); ?>/prod/js/theme.js?v=<?php echo $rand; ?>"></script>

<?php
}
?>
<!-- *** END IMPORT_FOOTER *** -->