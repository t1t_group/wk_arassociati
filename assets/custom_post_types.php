<?php
/***
*** ESEMPIO DICHIARAZIONE CUSTOM POST TYPE
***
function wk_create_post_type() {
	register_post_type('stage',
		array(
				'labels' => array(
				'name' => __( 'Stages', "webkolm" ),
				'singular_name' => __( 'Stage', "webkolm" )
			),
				'public' => true,
				'has_archive' => true,
				'supports' => array( 'title', 'editor', 'thumbnail'),
		)
	);
}


add_action( 'init', 'wk_create_post_type' );

/***
*** commento
***/

?>