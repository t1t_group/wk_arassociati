<!-- *** IMPORT_HEADER *** -->
<?php

if(is_user_logged_in()){
	$rand=rand(0,1000); ?>

	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/dev/css/core__normalize.css?v=<?php echo $rand; ?>">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/dev/css/core__main.css?v=<?php echo $rand; ?>">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/dev/css/core__grid.css?v=<?php echo $rand; ?>">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/dev/css/core__mediavisibility.css?v=<?php echo $rand; ?>">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/dev/css/third_parts/flexslider.css?v=<?php echo $rand; ?>">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/dev/css/layout__tipografia.css?v=<?php echo $rand; ?>">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/dev/css/layout__header.css?v=<?php echo $rand; ?>">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/dev/css/layout__menumobile.css?v=<?php echo $rand; ?>">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/dev/css/layout__menuwide.css?v=<?php echo $rand; ?>">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/dev/css/layout__single.css?v=<?php echo $rand; ?>">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/dev/css/vc_styles/slider.css?v=<?php echo $rand; ?>">

<?php 

} else { ?>
	<!-- ATF STILE -->
	<style type="text/css">
		<?php 
		echo file_get_contents(get_template_directory() . '/prod/css/atf.css'); ?>
	</style>

<?php
}
?>
<!-- *** END IMPORT_HEADER *** -->