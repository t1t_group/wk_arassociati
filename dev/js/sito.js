/* DIMENSIONI SCHERMO */
	 
	var $width = $(window).width();
	var $height = $(window).height();
	var $docheight = document.documentElement.scrollHeight;
	var $scrollspace = $docheight-$height;

function is_touch_device() {
 return (('ontouchstart' in window)
      || (navigator.maxTouchPoints > 0)
      || (navigator.msMaxTouchPoints > 0));
 //navigator.msMaxTouchPoints for microsoft IE backwards compatibility
}


$(document).ready(function() {

	// OTTIMIZZAZIONE PER RETINA */
	
	if (window.devicePixelRatio == 2) {

          var images = $("img.hires");

          // loop through the images and make them hi-res
          for(var i = 0; i < images.length; i++) {

            // create new image name
            var imageType = images[i].src.substr(-4);
            var imageName = images[i].src.substr(0, images[i].src.length - 4);
            imageName += "@x2" + imageType;

            //rename image
            images[i].src = imageName;
          }
     }
	 
	
	if(!is_touch_device()){
		
		$(".touch").addClass("notouch");
		$("body").addClass("notouch");
		
	}


		/***	MAIN MENU TOGGLE 	***/
		// Gestisce le funzionalità dell'header allo scroll
	  	function main_menu_toggle(scrollTop){
	  		// SE sono in cima alla pagina
	  		if(scrollTop < 60) {
	  			if(!$(".slides li.flex-active-slide").hasClass('slide-scura')){
		  			// header esteso
		  			
		  		}
	  			// SE in homepage
	  			if(document.querySelector('body').classList.contains('home')){
	  				// abilito la trasparenza dell'header
	  				document.querySelector('header').classList.add('trasparente');
	  			}

	  			document.querySelector('header').classList.remove('attivo');
	  		} else {
		  			// disattivo sempre la trasparenza
		  			document.querySelector('header').classList.remove('trasparente');
		  			// SE scroll verso l'alto
		  			/*if(scrollTop < oldScrollTop){
		  				// header esteso
		  				document.querySelector('header').classList.remove('attivo');
		  			} else {*/
	  				// header ristretto
	  				document.querySelector('header').classList.add('attivo');
	  			//}	
	  		}
	  		// aggiurno la variabile per direzione scroll
	  		oldScrollTop = window.pageYOffset;
	  	}

	  	var oldScrollTop = 0;
	  	//if($width>767){
	  		// init
	  		main_menu_toggle(window.pageYOffset);

	  		// HEADER menu toggle
	  		window.addEventListener('scroll', function() {
	  			main_menu_toggle(window.pageYOffset);
	  		});
	  	//}

	
	/* FORM */

	$('label.form-label').on('click', function(){
		$(this).siblings('.form-input').trigger('focus');
	});

	$('.form-input').focus(function(){
	  	$(this).parents('.form-group').addClass('focused');
	});

	$('.form-input').blur(function(){
		var inputValue = $(this).val();
		if ( inputValue == "" ) {
			$(this).removeClass('filled');
			$(this).parents('.form-group').removeClass('focused');  
		} else {
			$(this).addClass('filled');
		}
	});

	/***
     *  Funzione per inviare form (AJAX)
     ***/
    
    $("#form-id").on('submit', function(e){
    	e.preventDefault();

    	var form = $(this);

    	var results = form.serializeArray();

    	//if(validateForm(form)){

	        var request = $.ajax({
	            type: "POST",
	            url: ajaxurl, 
			    data: {
				    action: 'webkolm_ajax_form_submit',
				    content: results,
				},
	           
	            success: function(data){
	                $(form).children().fadeOut(500, function(){
	                	$(form).addClass('success').find('.sa').fadeIn(500, function(){
	                		$(this).addClass('active');
	                	});
	                });

	                //console.log(data);
	            }   
	        }); 
	    //} else {
	    	//console.log("error!!!");
	   //}
    });  



	
	if($width<1000) {
		
		/* PER SMARTPHONE */
		
		
		/* TASTO PER APERTURA MENU */
		$("a.mobile-menu").on('click', function (){
		  	$('body').toggleClass('nav-open');
		  	$("body").toggleClass("bloccoscroll");
			$("html").toggleClass("bloccoscroll");
		});
		
		$("nav.menu_mobile_nav .menu").on('click', 'li a', function(){
			var $this=$(this);
			if($this.hasClass("attivo"))
			{
				$this.toggleClass("attivo");
				$this.next('ul.sub-menu').slideToggle();
			}
			else{
				$this.next('ul.sub-menu').slideToggle();
				$this.toggleClass("attivo");	
			}
			
			if($(this).siblings("ul.sub-menu").size()!=0){
			  	return false;
			}
		});
		
		$("nav.menu_mobile_nav .menu li.current-menu-ancestor > a").addClass("attivo");
	} 

	/* SLIDER SITO */
	if($(".gallery-container").length > 0){
		$('.gallery-container').flexslider({
		    animation: "fade",
		    animationLoop: true,
		    slideshow: true,
		    slideshowSpeed : "4000",
		     pauseOnHover: true,
		    multipleKeyboard: true,
		    keyboard: true,
		    controlNav: true, 

		});
	}

	/* SLIDER SITO */
	if($(".wk-slider.wk_horizontal").length > 0){
		$('.wk-slider.wk_horizontal').flexslider({
		    animation: "fade",
		    animationLoop: true,
		    slideshowSpeed : "3000",
		     pauseOnHover: true,
		    multipleKeyboard: true,
		    keyboard: true
		});
	}

	/* SLIDER SITO */
	if($(".wk-slider.hero-slider-home").length > 0){
		$('.wk-slider.hero-slider-home').flexslider({
		    animation: "fade",
		    animationLoop: true,
		    slideshow: true,
		    video: true,
		    slideshowSpeed : "4000",
		    animationSpeed: 1000,
		    pauseOnHover: true,
		    multipleKeyboard: true,
		    keyboard: true,
		    before: function(slider){
			    	// Se la prossima slide ha testo nero cambio il colore del menu
			    	if(($(slider).offset().top == 0) && ($width>=1000)){
			    		var slides = $(slider).find('.slides li');
			    		if($(slides[slider.getTarget("next")]).hasClass('slide-scura')){
			    			$('header').addClass('bianco');
			    		} else {
			    			$('header.bianco').removeClass('bianco');
			    		}
			    	}

			    	if($(slider).find(".flex-active-slide video")[0]){
			    			var newVideo = document.querySelector(".hero-slider-home .flex-active-slide video");
			    			newVideo.pause();
			    	}
		    },
		    after: function(slider){
		    		if($(slider).find(".flex-active-slide video")[0]){
		    				var newVideo = document.querySelector(".hero-slider-home .flex-active-slide video");
		    				newVideo.play();
		    		}
		    },
		    start: function(slider){
		    	// Se la prossima slide ha testo nero cambio il colore del menu
	    		if($(".slides li.flex-active-slide").hasClass('slide-scura')){
	    			$('header').addClass('bianco');
	    		} else {
	    			$('header.bianco').removeClass('bianco');
	    		}
	        },
		});
	}

	/* ISOTOPE PER GRIGLIA DESIGNER */
	if($('.masonry-grid').length>0){
		var $grid = $('.masonry-grid').imagesLoaded( function() {
		  // init Isotope after all images have loaded
		  $grid.isotope({
		    // options...
		    itemSelector: '.item-masonry'
		  });
		});
	}


	$(".accordion_sidebar .title_accordion").on('click', function(){
		$(this).siblings('.galleria').find('a').first().trigger('click');
	});

	/* CAROUSEL POST

	$('.owl-carousel').owlCarousel({
	   loop:true,
	       margin:30,
	       responsiveClass:true,
	       autoplay:true,
	       dots:false,
	       nav:true,
	       navText:['&#60;','&#62;'],
	       responsive:{
	           0:{
	               items:1
	           },
	           600:{
	               items:2
	           },
	           1000:{
	               items:3
	           }
	           1280:{
	               items:4
	           }
	       }
	}); */
	
	// Fix slider load homepage
	const homeSliderCaptions = document.querySelectorAll('.slide__caption');

	homeSliderCaptions.forEach((e)=> {
		e.classList.add('wk-loaded');
	})

});	/* FINE DOCUMENT READY */



