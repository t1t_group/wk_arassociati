<div class="wrapper blocco_meta_progetto">
	<div class="blocco_meta__sx">
		<?php 
			// POSIZIONE GEOGRAFICA -> LOCALIZZAZIONE
			$term_list = wp_get_post_terms($post->ID, 'localizzazione');
			$term_list = array_reverse($term_list);
			foreach($term_list as $term){
				
				if($term->parent!=""){
					if($localita_str!= ""){
						$localita_str .= '<span class="separatore">, </span>';
					}
					$localita = traducistringa($term->name, ICL_LANGUAGE_CODE );
					$localita_str .= $localita;
				}
			}
		?>
		<h6 class=""><?php echo $localita_str; ?></h6>

		<?php
		// TIPOLOGIA DI PROGETTO
		$tipologia=get_the_terms($post->ID, 'tipologia-di-progetto');
		foreach ($tipologia as $tipo) {
			echo "<br>";
			echo '<h6 class="upper">'.$tipo->name.'</h6>';
		}

		// ANNO
		$periodo=types_render_field("anno-di-realizzazione", array("raw"=>"true"));
		if($periodo!=""){
			echo '<h6 class="upper">'.$periodo.'</h6>';
		}

		// COMMITTENTE
		$committente=types_render_field("committente", array("raw"=>"true"));
		if($committente!=""){
			echo "<br>";
			echo "<h6>". __('Client', 'arassociati') ."</h6>";
			echo '<h6 class="upper">'.$committente.'</h6>';
		}
		
		// COLLABORATZIONI
		$collaborazioni=types_render_field("collaborazioni", array("raw"=>"true"));
		if($collaborazioni!=""){
			echo "<br>";
			echo "<h6>". __('In collaboration with', 'arassociati') ."</h6>";
			echo '<span class="link_collaborazioni">'.$collaborazioni.'</span>';
		} ?>
	</div>

	<div class="blocco_meta__dx">
		<?php
		// GALLERIA TECNICA
		$gallery=types_render_field("galleria-tecnica", array('post_id' => $post->ID, "raw"=>"true"));

		if($gallery!=""){
			echo '<div class="accordion_sidebar">';
			echo '<h5 class="title_accordion">'.__('Technical drawings', 'arassociati').'
			<svg x="0px" y="0px" width="30px" height="30px" viewBox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve">
	            <g>
	              <path class="plus" d="M20.738,18.345h6.291v1.59h-6.291v6.844h-1.718v-6.844h-6.162v-1.59h6.162v-6.617h1.718V18.345z"/>
	            </g>
	            <g>
	              <path class="minus" d="M27.031,20H12.847v-1.635h14.185V20z"/>
	            </g>
	        </svg></h5>';
			echo do_shortcode($gallery);
			echo '</div>';
		} ?>	
	</div>
</div>