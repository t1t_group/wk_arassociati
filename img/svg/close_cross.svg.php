<svg version="1.1" id="close_cross" x="0px" y="0px"
	 viewBox="0 0 59.6 59.6" style="enable-background:new 0 0 59.6 59.6;" xml:space="preserve">
<line class="st0" style="fill:none;stroke:#000B27;stroke-width:2;stroke-linecap:round;stroke-miterlimit:10;" x1="57.6" y1="2" x2="2" y2="57.6"/>
<line class="st0" style="fill:none;stroke:#000B27;stroke-width:2;stroke-linecap:round;stroke-miterlimit:10;" x1="2" y1="2" x2="57.6" y2="57.6"/>
</svg>